{
   	"Query": [  
	
	{
 
 	 "variables" : [
              {
                  "alias": "y",
                  "name": "y",
                  "type": "integer",
                  "value": "10",  
                  "automata": "Automata1",
                  "operator": "="                   
                },
                {
                  "alias": "z",
                  "name": "z",
                  "type": "integer",
                  "value": "10",  
                  "automata": "Automata2",
                  "operator": "="                   
                }
              ],
        "locations":  [   
          {
              "alias": "A02",
              "loc": "2",
              "automata": "Automata0"
          },
          {
              "alias": "A12",
              "loc": "2",
              "automata": "Automata1"
          }
        ]
        "formula": "((A02 && y) || (A12 && z))"
      }   
	]
} 
