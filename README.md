CrEStO Experiments

The next experiments was made in the CrEStO framework, each folder corresponds to an complete experiment. 
Each one consist to two JSON format files with .txt extension; one for the network and other for the query. 
The rest files correspond to graphic verification to the properties obtained from the CREStO tool, two .xml files for the network original and transformed, 
and finally, two .q files for the queries, one for “no deadlock” query which the network can reach, and one for “error” query which can’t reach, these last works for the original 
and transformed network. The exercises were performed with the Uppaal tool 4.0.15 version.

For run them
You must download the Uppaal tool version 4.0.15. at http://www.uppaal.org/

By the console, go to the path where is located the Uppaal tool, and write the instruction “./uppaal”, the application will open with a blank document, 
through the toolbar select “file” tab, and then select "open system” , the document browser will open and you will have to access the path where the downloaded 
files are located and select the .xml file corresponding to the network. It will possible to observe automata network and their properties, in case of being a transformed network, 
the restrictions equivalent to the priorities obtained will be observed. To simulate the network behavior, select “Simulator” tab, can run network system step by step with “Next” button, 
and will be observed how the variable values change.
To test the query, select "Verifier" tab, then select "File" in the toolbar, and then select "Open queries". The query will be observed in the top box, to test it press "Check" button, 
in the under box it will be observed if the property is satisfactory or not.