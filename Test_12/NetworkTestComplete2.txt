{
  "System": [
    {
      "name": "SimpleNetwork",
      "processes": [
        {
          "name": "Automata0",
		  "variables": [
			{
              "name": "x",
              "type": "integer",
			  "controlled": "true" 
            }
		  ],
		  "labels": [
			{
              "name": "dummy" 
			  "strict": "true"  
            },
			{
              "name": "a" 
			  "strict": "true"  
            },
			{
              "name": "b" 
			  "strict": "true"  
            }
		  ],
          "locations": [
            {
              "name": "0",
              "invariant": "true",
              "initial": "false"
            },
            {
              "name": "1",
              "invariant": "true",
              "initial": "true"
            },
            {
              "name": "2",
              "invariant": "true",
              "initial": "false"
            }
          ],
          "edges": [
            {
              "origin": "0",
              "action": [
				{
                  "name": "dummy",
                  "type": "!"
                }
			  ],
			  "guard": [
				{
				  "expr": "true"
				}
			  ],
              "reset": [
				{
				  "expr": "x:= 0"
				}
			  ],
              "destination": "1"
            },
            {
              "origin": "1",
              "action": [
                {
                  "name": "a",
                  "type": "!"
                }
              ],
			  "guard": [
				{
				  "expr": "true"
				} 
			  ],
              "reset": [
				{
				  "expr": "x:=10"
				}
			  ],
              "destination": "2"
            },
            {
              "origin": "2",
              "action": [
                {
                  "name": "b",
                  "type": "!"
                }
              ],
			  "guard": [
				{
				  "expr": "true"
				}
			  ],
              "reset": [
				{
				  "expr": "x:=0"
				}
			  ],
              "destination": "1"
            }
          ]
        },
	    {
          "name": "Automata1",
		  "variables": [
			{
              "name": "y",
              "type": "integer",
			  "controlled": "true" 
            }
		  ],
		  "labels": [
			{
              "name": "dummy" 
			  "strict": "true"  
            },
			{
              "name": "c" 
			  "strict": "true"  
            },
			{
              "name": "d" 
			  "strict": "true"  
            }
		  ],
          "locations": [
            {
              "name": "0",
              "invariant": "true",
              "initial": "false"
            },
            {
              "name": "1",
              "invariant": "true",
              "initial": "true"
            },
            {
              "name": "2",
              "invariant": "true",
              "initial": "false"
            }
          ],
          "edges": [
            {
              "origin": "0",
              "action": [
				{
                  "name": "dummy",
                  "type": "!"
                }
			  ],
			  "guard": [
				{
				  "expr": "true"
				}
			  ],
              "reset": [
				{ 
				  "expr": "y:=0 "
				}
			  ],
              "destination": "1"
            },
            {
              "origin": "1",
              "action": [
                {
                  "name": "c",
                  "type": "!"
                }
              ],
			  "guard": [
				{
				  "expr": "true"
				}
			  ],
              "reset": [
				{
				  "expr": "y:=10"
				} 
			  ],
              "destination": "2"
            },
            {
              "origin": "2",
              "action": [
                {
                  "name": "d",
                  "type": "!"
                }
              ],
			  "guard": [
				{
				  "expr": "true"
				}
			  ],
              "reset": [
				{
				  "expr": "y:=0"
				}
			  ],
              "destination": "1"
            }
          ]
        },
	    {
          "name": "Automata2",
		  "variables": [
			{
              "name": "z",
              "type": "integer",
			  "controlled": "true" 
            }
		  ],
		  "labels": [
			{
              "name": "dummy" 
			  "strict": "true"  
            },
			{
              "name": "e" 
			  "strict": "true"  
            },
			{
              "name": "f" 
			  "strict": "true"  
            }
		  ],
          "locations": [
            {
              "name": "0",
              "invariant": "true",
              "initial": "false"
            },
            {
              "name": "1",
              "invariant": "true",
              "initial": "true"
            },
            {
              "name": "2",
              "invariant": "true",
              "initial": "false"
            }
          ],
          "edges": [
            {
              "origin": "0",
              "action": [
				{
                  "name": "dummy",
                  "type": "!"
                }
			  ],
			  "guard": [
				{
				  "expr": "true"
				}
			  ],
              "reset": [
				{ 
				  "expr": "z:=0 "
				}
			  ],
              "destination": "1"
            },
            {
              "origin": "1",
              "action": [
                {
                  "name": "e",
                  "type": "!"
                }
              ],
			  "guard": [
				{
				  "expr": "true"
				}
			  ],
              "reset": [
				{
				  "expr": "z:=10"
				} 
			  ],
              "destination": "2"
            },
            {
              "origin": "2",
              "action": [
                {
                  "name": "f",
                  "type": "!"
                }
              ],
			  "guard": [
				{
				  "expr": "true"
				}
			  ],
              "reset": [
				{
				  "expr": "z:=0"
				}
			  ],
              "destination": "1"
            }
          ]
        },
        {
          "name": "Automata3",
      "variables": [
      {
              "name": "j",
              "type": "integer",
        "controlled": "true" 
            }
      ],
      "labels": [
      {
              "name": "dummy" 
        "strict": "true"  
            },
      {
              "name": "g" 
        "strict": "true"  
            },
      {
              "name": "h" 
        "strict": "true"  
            }
      ],
          "locations": [
            {
              "name": "0",
              "invariant": "true",
              "initial": "false"
            },
            {
              "name": "1",
              "invariant": "true",
              "initial": "true"
            },
            {
              "name": "2",
              "invariant": "true",
              "initial": "false"
            }
          ],
          "edges": [
            {
              "origin": "0",
              "action": [
        {
                  "name": "dummy",
                  "type": "!"
                }
        ],
        "guard": [
        {
          "expr": "true"
        }
        ],
              "reset": [
        { 
          "expr": "j:=0 "
        }
        ],
              "destination": "1"
            },
            {
              "origin": "1",
              "action": [
                {
                  "name": "g",
                  "type": "!"
                }
              ],
        "guard": [
        {
          "expr": "true"
        }
        ],
              "reset": [
        {
          "expr": "j:=10"
        } 
        ],
              "destination": "2"
            },
            {
              "origin": "2",
              "action": [
                {
                  "name": "h",
                  "type": "!"
                }
              ],
        "guard": [
        {
          "expr": "true"
        }
        ],
              "reset": [
        {
          "expr": "j:=0"
        }
        ],
              "destination": "1"
            }
          ]
        }
      ]
    }  
  ]
}
